USERS LIST

Authored by Chaitanya Dessai

This module generates various user lists that can be viewed as pages or blocks.
The user lists can be organized alphabetically from A-Z, 
by the most recent users, or by users who have contributed 
content of a specific type. Additionally, lists can be filtered by user role.

INSTALLATION:

Like any other Drupal contributed module using composer command.
