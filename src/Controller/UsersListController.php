<?php

namespace Drupal\users_list\Controller;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Controller for users list pages.
 */
class UsersListController extends ControllerBase {

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The config factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The default number of users to list.
   *
   * @var int
   */
  protected $defaultNumberOfUsers;

  /**
   * Constructs a UsersListController object.
   *
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory service.
   */
  public function __construct(Connection $database, EntityTypeManagerInterface $entity_type_manager, ConfigFactoryInterface $config_factory) {
    $this->database = $database;
    $this->entityTypeManager = $entity_type_manager;
    $this->configFactory = $config_factory;
    $this->defaultNumberOfUsers = $this->configFactory->get('users_list.settings')->get('users_list_default_number') ?? 10;
  }

  /**
   * Render users list based on type.
   */
  public function usersList($type) {
    // Determine the type of users list to query.
    switch ($type) {
      case 'alpha':
        $result = $this->getUsersByAlpha();
        $header = $this->t('Users Beginning with A-Z');
        $empty_msg = $this->t('There are currently no users with usernames beginning with A-Z.');
        break;

      case 'content':
        $entityTypeManager = $this->entityTypeManager;
        $types = [];
        $content_types = $entityTypeManager->getStorage('node_type')->loadMultiple();
        foreach ($content_types as $contentType) {
          $types[] = $contentType->id();
        }
        $result = $this->getUsersByContent($types);
        $content_type_names = implode(', ', $types);
        $header = $this->t('List of users who have created nodes at least one of the following content types: @content', ['@content' => $content_type_names]);
        $empty_msg = $this->t('There are currently no users who have contributed at least one of the following content types: @content.', ['@content' => $content_type_names]);
        break;

      case 'roles':
        $entityTypeManager = $this->entityTypeManager;
        $role_type = [];

        // Load all roles.
        $roles = $entityTypeManager->getStorage('user_role')->loadMultiple();
        foreach ($roles as $role) {
          $role_type[] = $role->id();
        }

        $result = $this->getUsersByRoles($role_type);

        // Joining role names for display purposes.
        $role_names = implode(', ', $role_type);
        $header = $this->t('Site users with any of the following roles: @roles', ['@roles' => $role_names]);
        $empty_msg = $this->t('There are currently no users with any of the following roles: @roles.', ['@roles' => $role_names]);
        break;

      case 'newest':
        $result = $this->getNewestUsers();
        $header = $this->t('Newest Users');
        $empty_msg = $this->t('There are currently no users.');
        break;

      case 'other':
        $result = $this->getUsersNotInAlpha();
        $header = $this->t('Users Not in A-Z');
        $empty_msg = $this->t('There are currently no users with usernames not beginning with A-Z.');
        break;

      default:
        $result = $this->getAllUsers();
        $header = $this->t('All Users');
        $empty_msg = $this->t('There are currently no users.');
    }

    $users = [];
    foreach ($result as $record) {
      $users[] = [
        'uid' => $record->uid,
        'name' => $record->name,
        'roles' => isset($record->roles_target_id) ? $record->roles_target_id : [],
      ];
    }

    $build = [
      '#theme' => 'users_list',
      '#header' => $header,
      '#users' => $users,
      '#empty_msg' => empty($users) ? $empty_msg : '',
      '#cache' => [
        'tags' => ['config:users_list.settings'],
        'contexts' => ['user.roles'],
      ],
    ];

    return $build;
  }

  /**
   * Query to get users alphabetically.
   */
  protected function getUsersByAlpha() {
    $query = $this->database->select('users_field_data', 'u')
      ->fields('u', ['uid', 'name'])
      ->condition('u.status', 1)
      ->orderBy('u.name', 'ASC')
      ->range(0, $this->defaultNumberOfUsers);
    return $query->execute()->fetchAll();
  }

  /**
   * Query to get users by content type.
   */
  protected function getUsersByContent($types) {
    $query = $this->database->select('users_field_data', 'u');
    $query->distinct();
    $query->innerJoin('node_field_data', 'n', 'n.uid = u.uid');
    $query->fields('u', ['uid', 'name']);
    $query->condition('u.status', 1);

    // Use 'IN' operator when passing an array of types.
    $query->condition('n.type', $types, 'IN');

    $query->orderBy('u.name', 'ASC')
      ->range(0, $this->defaultNumberOfUsers);

    return $query->execute()->fetchAll();
  }

  /**
   * Query to get users by role.
   */
  protected function getUsersByRoles(array $role_type) {
    $query = $this->database->select('users_field_data', 'u');
    $query->distinct();
    $query->fields('u', ['uid', 'name']);
    $query->condition('u.status', 1);
    $query->innerJoin('user__roles', 'r', 'r.entity_id = u.uid');
    // Use 'IN' operator for array of roles.
    $query->fields('r', ['roles_target_id']);
    $query->condition('r.roles_target_id', $role_type, 'IN');
    $query->orderBy('u.name', 'ASC')
      ->range(0, $this->defaultNumberOfUsers);
    return $query->execute()->fetchAll();
  }

  /**
   * Query to get newest users.
   */
  protected function getNewestUsers() {
    $query = $this->database->select('users_field_data', 'u')
      ->fields('u', ['uid', 'name'])
      ->condition('u.status', 1)
      ->orderBy('u.created', 'DESC')
      ->range(0, $this->defaultNumberOfUsers);
    return $query->execute()->fetchAll();
  }

  /**
   * Query to get users not in A-Z.
   */
  protected function getUsersNotInAlpha() {
    $query = $this->database->select('users_field_data', 'u')
      ->fields('u', ['uid', 'name'])
      ->condition('u.status', 1)
      ->where('LOWER(u.name) < :a OR LOWER(u.name) > :z', [':a' => 'a', ':z' => 'z'])
      ->orderBy('u.name', 'ASC')
      ->range(0, $this->defaultNumberOfUsers);
    return $query->execute()->fetchAll();
  }

  /**
   * Query to get all users.
   */
  protected function getAllUsers() {
    $query = $this->database->select('users_field_data', 'u')
      ->fields('u', ['uid', 'name'])
      ->condition('u.status', 1)
      ->orderBy('u.name', 'ASC')
      ->range(0, $this->defaultNumberOfUsers);
    return $query->execute()->fetchAll();
  }

}
