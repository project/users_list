<?php

namespace Drupal\users_list\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure users list settings.
 */
class UsersListSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['users_list.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'users_list_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('users_list.settings');

    $form['users_list_default_number'] = [
      '#type' => 'number',
      '#title' => $this->t('Default number of users to list'),
      '#default_value' => $config->get('users_list_default_number') ?? 10,
      '#required' => TRUE,
    ];

    $form['users_list_embed_menu'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Embed users listing page menu'),
      '#default_value' => $config->get('users_list_embed_menu') ?? TRUE,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $this->configFactory->getEditable('users_list.settings')
      ->set('users_list_default_number', $form_state->getValue('users_list_default_number'))
      ->set('users_list_embed_menu', $form_state->getValue('users_list_embed_menu'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
