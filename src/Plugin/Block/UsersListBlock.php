<?php

namespace Drupal\users_list\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\users_list\Controller\UsersListController;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'Users List' Block.
 *
 * @Block(
 *   id = "users_list_block",
 *   admin_label = @Translation("Users List Block"),
 *   category = @Translation("Custom")
 * )
 */
class UsersListBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The users list controller.
   *
   * @var \Drupal\users_list\Controller\UsersListController
   */
  protected $usersListController;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Constructs a new UsersListBlock instance.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\users_list\Controller\UsersListController $usersListController
   *   The users list controller.
   * @param \Drupal\Core\Session\AccountProxyInterface $currentUser
   *   The current user.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, UsersListController $usersListController, AccountProxyInterface $currentUser) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->usersListController = $usersListController;
    $this->currentUser = $currentUser;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('controller.users_list'),
      $container->get('current_user')
    );
  }

  /**
   * {@inheritdoc}
   */

  /**
   * {@inheritdoc}
   */
  public function build() {
    // Check if the user is anonymous.
    if ($this->currentUser->isAnonymous()) {
      return [
        '#markup' => $this->t('Access denied.'),
      ];
    }
    $type = $this->configuration['users_list_type'];
    $build = $this->usersListController->usersList($type);

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);

    $form['users_list_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Users List Type'),
      '#options' => [
        'alpha' => $this->t('Alphabetical'),
        'content' => $this->t('Content Types'),
        'roles' => $this->t('Roles'),
        'newest' => $this->t('Newest Users'),
        'other' => $this->t('Users Not in A-Z'),
        'all' => $this->t('All Users'),
      ],
      '#default_value' => $this->configuration['users_list_type'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state): void {
    parent::blockSubmit($form, $form_state);
    $this->configuration['users_list_type'] = $form_state->getValue('users_list_type');
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'users_list_type' => 'all',
    ];
  }

}
